export class User {
  name: string;
  surname: string;
  region: string;
  checked: boolean;
}
