import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Details} from '../../entities/details';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnChanges {

  @Input()
  public names: string[];

  url = 'https://api.duckduckgo.com/?format=json&pretty=1&q=';

  details: Details;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    if (this.names) {
      this.http.get(this.url + this.names.join('+')).subscribe(value => this.details = this.extractDetails(value));
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.names = changes['names'].currentValue;
    if (this.names !== undefined && this.names.length !== 0) {
      this.http.get(this.url + this.names.join('+')).subscribe(value => this.details = this.extractDetails(value));
    } else {
      this.details = null;
    }
  }

  public extractDetails(data: Object): Details {
    let detail = null;
    if (data && data['RelatedTopics'][0]) {
      detail = new Details();
      const relatedTopics = data['RelatedTopics'][0];
      if (relatedTopics['FirstURL']) {
        detail.url = relatedTopics['FirstURL'];
      }
      if (relatedTopics['Icon']) {
        detail.icon = relatedTopics['Icon']['URL'];
      }
      if (relatedTopics['Text']) {
        detail.text = relatedTopics['Text'];
      }
    }
    return detail;
  }

}
