import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DataTablesModule} from 'angular-datatables';

import {DetailsComponent} from './details.component';
import {Details} from '../../entities/details';
import {HttpClientModule} from '@angular/common/http';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  const demoResponse = {
    'Heading': 'Florentin',
    'AbstractSource': 'Wikipedia',
    'ImageWidth': 0,
    'Answer': '',
    'ImageIsLogo': 0,
    'Redirect': '',
    'Image': '',
    'ImageHeight': 0,
    'Entity': '',
    'DefinitionSource': '',
    'AbstractURL': 'https://en.wikipedia.org/wiki/Florentin',
    'Abstract': '',
    'Results': [],
    'Infobox': '',
    'RelatedTopics': [
      {
        'Text': 'Florentin Pogba A Guinean international footballer currently playing as a defender for Saint-\u00c9tienne in Ligue 1.',
        'Result': '<a href="https://duckduckgo.com/Florentin_Pogba">Florentin Pogba</a> A Guinean international footballer.',
        'FirstURL': 'https://duckduckgo.com/Florentin_Pogba',
        'Icon': {
          'Height': '',
          'Width': '',
          'URL': 'https://duckduckgo.com/i/4e711d3a.jpg'
        }
      }],
  };

  const demoResult = new Details();
  demoResult.url = 'https://duckduckgo.com/Florentin_Pogba';
  demoResult.text = 'Florentin Pogba A Guinean international footballer currently playing as a defender for Saint-\u00c9tienne in Ligue 1.';
  demoResult.icon = 'https://duckduckgo.com/i/4e711d3a.jpg';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsComponent],
      imports: [
        DataTablesModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('extractDetails should return null', () => {
    const result = component.extractDetails(null);
    expect(result).toBe(null);
  });

  it('extractDetails should return result', () => {
    const result = component.extractDetails(demoResponse);
    expect(result.icon).toBe(demoResult.icon);
    expect(result.text).toBe(demoResult.text);
    expect(result.url).toBe(demoResult.url);
  });

});
