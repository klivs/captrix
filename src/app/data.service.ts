import {Injectable} from '@angular/core';
import {User} from '../entities/user';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  users = [];

  constructor() {}

  getByGender(gender: string) {
    return this.users[gender];
  }

  setByGender(gender: string, users: User[]) {
    this.users[gender] = users;
  }
}
