import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DetailsComponent} from './details/details.component';
import {DataTablesModule} from 'angular-datatables';
import {UsertableComponent} from './usertable/usertable.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    UsertableComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    DataTablesModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
