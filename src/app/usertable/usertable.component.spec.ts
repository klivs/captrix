import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UsertableComponent} from './usertable.component';
import {DataTablesModule} from 'angular-datatables';
import {DetailsComponent} from '../details/details.component';
import {HttpClientModule} from '@angular/common/http';

describe('UsertableComponent', () => {
  let component: UsertableComponent;
  let fixture: ComponentFixture<UsertableComponent>;

  const demoResponse = [{'name': 'Margot De', 'surname': 'Smet', 'gender': 'female', 'region': 'Belgium'}, {
    'name': 'Madison',
    'surname': 'Wilkinson',
    'gender': 'female',
    'region': 'England'
  }, {'name': 'Liana', 'surname': 'Vlk', 'gender': 'female', 'region': 'Slovakia'}, {
    'name': 'Corien',
    'surname': 'Smeets',
    'gender': 'female',
    'region': 'Netherlands'
  }, {'name': 'Alis', 'surname': 'Badea', 'gender': 'female', 'region': 'Romania'}, {
    'name': 'ლელა',
    'surname': 'მახარაძე',
    'gender': 'female',
    'region': 'Georgia'
  }, {'name': 'Dorothy', 'surname': 'Stewart', 'gender': 'female', 'region': 'United States'}, {
    'name': 'Bakai',
    'surname': 'Sári',
    'gender': 'female',
    'region': 'Hungary'
  }, {'name': 'Katie', 'surname': 'Cook', 'gender': 'female', 'region': 'England'}, {
    'name': 'Gauri',
    'surname': 'Dhami',
    'gender': 'female',
    'region': 'Nepal'
  }];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsertableComponent, DetailsComponent],
      imports: [
        DataTablesModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsertableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('extractUsers should return empty array', () => {
    const result = component.extractUsers(null);
    expect(result.length).toBe(0);
  });

  it('extractDetails should return result', () => {
    const result = component.extractUsers(demoResponse);
    expect(result.length).toBe(10);
  });

});
