import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../entities/user';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {DataService} from '../data.service';

@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css']
})
export class UsertableComponent implements OnInit, AfterViewInit, OnChanges {

  dtOptions: DataTables.Settings;

  dtTrigger = new Subject();

  users: User[];

  @Input() gender: string;

  usersUrl = 'https://uinames.com/api/?amount=10&gender=';

  selectedNames: string[];

  constructor(private data: DataService, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.users = this.data.getByGender(this.gender);
    if (this.users === undefined) {
      this.http.get(this.usersUrl + this.gender).subscribe(value => {
        this.users = this.extractUsers(value as Object[]);
        this.data.setByGender(this.gender, this.users);
      });
    }
    this.dtTrigger.next();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  public extractUsers(data: Object[]): User[] {
    const users = [];
    if (data) {
      for (let i = 0; i < data.length; i++) {
        const user = new User();
        user.name = data[i]['name'];
        user.surname = data[i]['surname'];
        user.region = data[i]['region'];
        user.checked = false;
        users.push(user);
      }
    }
    return users;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dtTrigger.next();
  }

  getSelectedUsers($event) {
    const selectedNames = [];
    $('input:checkbox[name=usersCheckbox]:checked').each(function () {
      selectedNames.push($(this).val());
    });
    this.selectedNames = selectedNames;
  }

}
