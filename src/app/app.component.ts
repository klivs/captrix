import {Component, OnInit} from '@angular/core';
import {UserTableData} from '../entities/UserTableData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Welcome to CAPtrix!';

  subtitle = 'Candidate Assessment Project';

  tabsData: UserTableData[];

  constructor() {}

  ngOnInit() {
    this.tabsData = [{
      title: 'Males',
      gender: 'male',
    }, {
      title: 'Females',
      gender: 'female',
    }];
  }
}
